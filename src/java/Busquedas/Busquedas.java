/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Busquedas;

import Organizacion.ClaseConexion;
import Organizacion.ClaseConsultas;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author PC
 */
public class Busquedas extends HttpServlet {

    public ClaseConsultas con;
    //resultado de la consulta
    //objeto de la clase conexion
    public ClaseConexion ConexionBD = new ClaseConexion();
    private String consulta;
    private ResultSet resultado;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject jsonObject = new JSONObject();
        JSONArray arrayData = new JSONArray();
        boolean Banderaregistros = false;



        int Caso = Integer.parseInt(request.getParameter("Caso"));

        //caso 1 buscar todas las facultades
        //caso 2 buscar todas las programas
        //caso 3 buscar todas las materias




        switch (Caso) {
            case 1://buscar todas las facultades
                System.out.println("entra a buscar todas las facultades");

                consulta = "select Nombre FROM Facultad ";
                resultado = consultar(consulta);

                if (resultado != null) {

                    while (resultado.next()) {
                        try {
                            //Compara dos cadenas de caracteres omitiendo si los caracteres están en mayúsculas o en minúsculas.

                            String Nombre = resultado.getString("Nombre");
                            //imprimiendo en el apache
                            System.out.println(Nombre);
                            JSONObject Objetosjson = new JSONObject();
                            Objetosjson.put("Nombre", Nombre);
                            arrayData.put(Objetosjson);



                        } catch (JSONException ex) {
                            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                        }


                    }
                    //enviando json
                    response.getWriter().write(arrayData.toString());


                } else {
                    try {
                        //enviamos un json  con nombre = no para saber que no hay registros
                        System.out.println("error");
                        jsonObject.put("Nombre", "no");
                        arrayData.put(jsonObject);
                        response.getWriter().write(arrayData.toString());
                    } catch (JSONException ex) {
                        Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                    }



                }


                break;
            case 2://buscar todas los programas

                //obtengo el valor del parametro  
                String facultadSelec = request.getParameter("Facultad");

                /*hay que buscar todos los programas que esten relacionado con esta facultad*/


                //primera consulta para buscar el id de la facultad
                consulta = "select ID FROM Facultad WHERE Nombre ='" + facultadSelec + "'";
                resultado = consultar(consulta);
                //segunda consulta ya encontrado el id buscar los programas que esten relacionado
                resultado.next();
                consulta = "select Nombre FROM Programa WHERE ID_FacultadFk='" + resultado.getString("ID") + "'";

                resultado = consultar(consulta);

                if (resultado != null) {

                    while (resultado.next()) {
                        try {

                            String Nombre = resultado.getString("Nombre");
                            //imprimiendo en el apache
                            System.out.println(Nombre);
                            JSONObject Objetosjson = new JSONObject();
                            Objetosjson.put("Nombre", Nombre);
                            arrayData.put(Objetosjson);

                        } catch (JSONException ex) {
                            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                        }


                    }
                    //enviando json
                    response.getWriter().write(arrayData.toString());


                } else {
                    try {
                        //enviamos un json  con nombre = no para saber que no hay registros
                        jsonObject.put("Nombre", "no");
                        arrayData.put(jsonObject);
                        response.getWriter().write(arrayData.toString());
                    } catch (JSONException ex) {
                        Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
            case 3://buscar todos las materias que esten relacionado con este programa

                //obtengo el valor del parametro  
                String programaSelec = request.getParameter("Programa");

                /*hay que buscar todos las materias que esten relacionado con este programa*/


                //primera consulta para buscar el id de la facultad
                consulta = "select ID FROM Programa WHERE Nombre ='" + programaSelec + "'";
                resultado = consultar(consulta);
                //segunda consulta ya encontrado el id buscar los programas que esten relacionado
                resultado.next();
                consulta = "select Nombre FROM Materia WHERE ID_ProgramaFk='" + resultado.getString("ID") + "'";

                resultado = consultar(consulta);

                if (resultado != null) {

                    while (resultado.next()) {
                        try {

                            String Nombre = resultado.getString("Nombre");
                            //imprimiendo en el apache
                            System.out.println(Nombre);
                            JSONObject Objetosjson = new JSONObject();
                            Objetosjson.put("Nombre", Nombre);
                            arrayData.put(Objetosjson);

                        } catch (JSONException ex) {
                            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                        }


                    }
                    //enviando json
                    response.getWriter().write(arrayData.toString());


                } else {
                    try {
                        //enviamos un json  con nombre = no para saber que no hay registros
                        jsonObject.put("Nombre", "no");
                        arrayData.put(jsonObject);
                        response.getWriter().write(arrayData.toString());
                    } catch (JSONException ex) {
                        Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
            case 4://busqueda de  documentos


                /*
                 //obtengo el valor del parametro  
                 String Facultad = request.getParameter("Programa");
                 String programa = request.getParameter("Programa");
                 String Materia = request.getParameter("Programa");

                 //hay que buscar todos las materias que esten relacionado con este programa


                 //primera consulta para buscar el id de la facultad
                 consulta = "select ID FROM Programa WHERE Nombre ='" + programaSelec + "'";
                 resultado = consultar(consulta);
                 //segunda consulta ya encontrado el id buscar los programas que esten relacionado
                 resultado.next();
                 consulta = "select Nombre FROM Materia WHERE ID_ProgramaFk='" + resultado.getString("ID") + "'";

                 resultado = consultar(consulta);

                 if (resultado != null) {

                 while (resultado.next()) {
                 try {

                 String Nombre = resultado.getString("Nombre");
                 //imprimiendo en el apache
                 System.out.println(Nombre);
                 JSONObject Objetosjson = new JSONObject();
                 Objetosjson.put("Nombre", Nombre);
                 arrayData.put(Objetosjson);

                 } catch (JSONException ex) {
                 Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                 }


                 }
                 //enviando json
                 response.getWriter().write(arrayData.toString());


                 } else {
                 try {
                 //enviamos un json  con nombre = no para saber que no hay registros
                 jsonObject.put("Nombre", "no");
                 arrayData.put(jsonObject);
                 response.getWriter().write(arrayData.toString());
                 } catch (JSONException ex) {
                 Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                 }

                 }

                
                 */
                break;

            case 5://Buscar la materia del docente activo
                //obtengo el valor del parametro  
                String docenteSelec = request.getParameter("Docente");

                /*hay que buscar todos las materias que esten relacionado con este Docente*/


                //primera consulta para buscar el id del docente
                consulta = "select ID FROM Docente WHERE Nombre ='" + docenteSelec + "'";
                resultado = consultar(consulta);

                //segunda consulta ya encontrado el id buscar las materia que esten relacionado con este docente
                resultado.next();

                System.out.println("docente");
                consulta = "select Nombre FROM Materia WHERE ID_DocenteFk='" + resultado.getString("ID") + "'";

                resultado = consultar(consulta);

                if (resultado != null) {

                    while (resultado.next()) {
                        try {

                            String Nombre = resultado.getString("Nombre");
                            //imprimiendo en el apache
                            System.out.println(Nombre);
                            JSONObject Objetosjson = new JSONObject();
                            Objetosjson.put("Nombre", Nombre);
                            arrayData.put(Objetosjson);

                        } catch (JSONException ex) {
                            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                        }


                    }
                    //enviando json
                    response.getWriter().write(arrayData.toString());


                } else {
                    try {
                        //enviamos un json  con nombre = no para saber que no hay registros
                        jsonObject.put("Nombre", "no");
                        arrayData.put(jsonObject);
                        response.getWriter().write(arrayData.toString());
                    } catch (JSONException ex) {
                        Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                break;
            case 6:
                //buscar todas Fotocopiadoras 

                /*hay que buscar todos las materias que esten relacionado con este Docente*/


                //primera consulta para buscar el id del docente
                consulta = "select Nombre FROM Fotocopiadora";
                resultado = consultar(consulta);
                if (resultado != null) {

                    while (resultado.next()) {
                        try {

                            String Nombre = resultado.getString("Nombre");
                            //imprimiendo en el apache
                            System.out.println(Nombre);
                            JSONObject Objetosjson = new JSONObject();
                            Objetosjson.put("Nombre", Nombre);
                            arrayData.put(Objetosjson);

                        } catch (JSONException ex) {
                            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                        }


                    }
                    //enviando json
                    response.getWriter().write(arrayData.toString());


                } else {
                    try {
                        //enviamos un json  con nombre = no para saber que no hay registros
                        jsonObject.put("Nombre", "no");
                        arrayData.put(jsonObject);
                        response.getWriter().write(arrayData.toString());
                    } catch (JSONException ex) {
                        Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

                break;
            case 7:

                //obtengo el valor del parametro  

                String NombreDocumento = request.getParameter("NombreDocumento");
                String Materia = request.getParameter("Materia");
                String Fotocopiadora = request.getParameter("Fotocopiadora");
                Date fecha = null; 
                int pg =0;
                int valor=0;
                
                String consulta1 = "select ID FROM Materia WHERE Nombre ='" +Materia + "'";
               ResultSet resultado1 = consultar(consulta1);
                String consulta2 = "select ID FROM Fotocopiadora WHERE Nombre ='" +Fotocopiadora + "'";
                ResultSet resultado2 = consultar(consulta2); 
                //segunda consulta ya encontrado el id buscar las materia que esten relacionado con este docente
                resultado1.next();
                resultado2.next();

                
             
                consulta =" INSERT INTO Documentos (Nombre, FechaEntrada,fechaSalida, NumeroDePag,Valor,Archivo,ID_FotocopiadoraFk,ID_MateriaFk) VALUES ('" + NombreDocumento + "','" + fecha + "','" + null + "','" + pg+ "','" + valor + "','" + null + "','" + resultado2.getString("ID")+ "','" + resultado1.getString("ID")+ "')";
                resultado = consultar(consulta);
               
                break;
            default:
                throw new AssertionError();
        }
    }

    ResultSet consultar(String consulta) {
        ResultSet respuesta = null;


        Connection conexion = ConexionBD.GetConexion();
        if (conexion != null) {
            //creamos el objeto llenando en constructor
            con = new ClaseConsultas(conexion, consulta);

            if (con.getError() == null) {

                respuesta = con.getResultado();

            } else {
                System.out.println("Error de consulta o no arrastro ningun Dato");
            }


        } else {
            System.out.println("No hay Conexión");
        }


        return respuesta;

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Busquedas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
