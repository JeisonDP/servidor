/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Docente;


import Organizacion.ClaseConexion;
import Organizacion.ClaseConsultas;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author PC
 */
public class loginDocente extends HttpServlet {

    
     public ClaseConsultas con;
    //resultado de la consulta
    //objeto de la clase conexion
    public ClaseConexion ConexionBD = new ClaseConexion();
    private String consulta;
    private ResultSet resultado;
    private  boolean Banderaregistros = false;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject jsonObject = new JSONObject();
        JSONArray arrayData = new JSONArray();
        //buscar un docente especifico
        try {

            String Nick = request.getParameter("email");
            String Clave = request.getParameter("password");


            consulta = "select Nombre, Clave FROM Docente WHERE Nombre = '" + Nick + "' and Clave = '" + Clave + "'";
            resultado = consultar(consulta);


            if (resultado != null) {
                while (resultado.next()) {


                    //hay registros despues que entre en el while
                    Banderaregistros = true;

                    String Nombre = resultado.getString("Nombre");
                    String pw = resultado.getString("Clave");

                    try {
                        //Compara dos cadenas de caracteres omitiendo si los caracteres están en mayúsculas o en minúsculas.
                        if (Nombre.equalsIgnoreCase(Nick) && pw.equalsIgnoreCase(Clave)) {
                            jsonObject.put("logeado", "si");
                            arrayData.put(jsonObject);

                        }
                    } catch (JSONException ex) {
                        System.out.println("error");
                        Logger.getLogger(loginDocente.class.getName()).log(Level.SEVERE, null, ex.getMessage() + "kkkkkkk");
                    }
                    response.getWriter().write(arrayData.toString());

                }
            } else {
                try {
                    //enviamos un json  con error si porque hay problemas en la consulta
                    System.out.println("error2");
                    jsonObject.put("logeado", "no");
                    arrayData.put(jsonObject);
                    response.getWriter().write(arrayData.toString());

                } catch (JSONException ex) {
                    Logger.getLogger(loginDocente.class.getName()).log(Level.SEVERE, null, ex);
                }

                response.getWriter().write(arrayData.toString());

            }
        } finally {
        }
        //_---------------------------------
        if (Banderaregistros) {
            System.out.println("hay registros");
        } else {

            try {
                jsonObject.put("logeado", "no");
            } catch (JSONException ex) {
                Logger.getLogger(loginDocente.class.getName()).log(Level.SEVERE, null, ex);
            }
            arrayData.put(jsonObject);
            JSONObject Objetosjson = new JSONObject();
            response.getWriter().write(arrayData.toString());
            System.out.println("no hay registros");

        }



    }
     ResultSet consultar(String consulta) {
        ResultSet respuesta = null;


        Connection conexion = ConexionBD.GetConexion();
        if (conexion != null) {
            //creamos el objeto llenando en constructor
            con = new ClaseConsultas(conexion, consulta);

            if (con.getError() == null) {

                respuesta = con.getResultado();

            } else {
                System.out.println("Error de consulta o no arrastro ningun Dato");
            }


        } else {
            System.out.println("No hay Conexión");
        }


        return respuesta;

    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(loginDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(loginDocente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
