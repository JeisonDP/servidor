package Organizacion;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author PC
 */
public class ClaseConsultas {

    private Connection con;
    private ResultSet rs;
    private ResultSetMetaData mtd;
    private String[] columnas;
    private String consulta, error;

    public ClaseConsultas(Connection Con, String sql) {
        this.con = Con;
        this.consulta = sql;
        try {

            Statement st = con.createStatement();
            rs = st.executeQuery(consulta);
            mtd = rs.getMetaData();
            error = null;


        } catch (SQLException ex) {
            error = ex.getMessage();
        }
    }

    public String getError() {

        return this.error;

    }

    public ResultSet getResultado() {

        return this.rs;
    }

    public String[] getNombreColumnas() {

        int numColumnas;
        try {
            numColumnas = mtd.getColumnCount();
            this.columnas = new String[numColumnas];

            for (int i = 0; i < numColumnas; i++) {

                columnas[i] = mtd.getColumnLabel(i + 1);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ClaseConsultas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return columnas;
    }

  
}
