<%-- 
    Document   : index
    Created on : 15-abr-2014, 21:40:54
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Proyecto Servidor de Documentos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/bootstrap-theme.css"/> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/Metodos.js"></script>
        <link href="css/style.css" rel="stylesheet" />


    </head>
    <body BACKGROUND="img/fondo5.jpg">
        <div class="container">
            <div >
                <div class="col-md-12 column">
                    <h3 class="text-center">
                        <span class="blanco"><img src="img/muchos.gif" width="201" height="201"  >Servidor Documentos</span> </h3>
                    <b><p>pensando en tu necesidad ! </p></b> 
                </div>
                <div class="rowclearfix" background-color:yellow>
                    <div class="col-md-12 column">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                                <a class="navbar-brand" href="#" onclick="Inicio()">Inicio</a>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">

                                    <li >  <!class="active">
                                    <a href=" #" onclick="Estudiante()">Estudiantes</a>

                                    </li>
                                    <li>
                                        <a href="#"onclick="Docente()">Docente</a>
                                    </li>
                                    <li>
                                        <a href="#"onclick="loginAdministrador()">Administrador</a>
                                    </li>


                                </ul>


                            </div>
                    </div>
                    <div class="Inicio">
                        <div class="rowclearfix2">
                            <div class="3_column">
                                <div class="row">
                                    <div class="col-md-4">


                                        <div class="subtítulo">
                                            <div class="thumbnail">
                                                <img alt="200x50" src="http://lorempixel.com/600/200/city">	
                                                <div class="caption">
                                                    <h3>

                                                        Estudiante
                                                    </h3>
                                                    <p>
                                                        hablaremos un poco de los Estudiante
                                                    </p>
                                                    <p>
                                                        <a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <img alt="100x200" src="http://lorempixel.com/600/200/city">
                                            <div class="caption">
                                                <h3>
                                                    Docentes
                                                </h3>
                                                <p>
                                                    hablaremos un poco de los docentes
                                                </p>
                                                <p>
                                                    <a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="thumbnail">
                                            <img alt="300x200" src="http://lorempixel.com/600/200/sports">
                                            <div class="caption">
                                                <h3>
                                                    Centros
                                                </h3>
                                                <p>
                                                    Hablaremos un poco de los Centros
                                                </p>
                                                <p>
                                                    <a class="btn btn-primary" href="#">Action</a> <a class="btn" href="#">Action</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul>

                        </div>
                    </div>
                </div>


                <div class="cajaEstudiante">

                    <b><span class="azul"><h1>Buscar :</h1></span></b>
                    <input type="text"   class="form-control" placeholder="Buscar docentes" >

                    <!busqueda avanzada >
                    <div class="panel-group" id="panel-253742">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="panel-title collapsed" onclick="BusquedaAvanzada()" data-toggle="collapse" data-parent="#panel-253742" href="#panel-element-676524">Busqueda avanzada</a>
                            </div>
                            <div id="panel-element-676524" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <form name="formComboPrograma" action="">
                                        <div >
                                            Facultad:
                                            <select  id="SelectFacultad" onchange="Seleccionandofacultad(this);"  class="selectpicker show-tick form-control" > </select>
                                        </div>
                                        <div >
                                            Programa:
                                            <select  id="SelectPrograma" onchange="SeleccionandoPrograma(this);" class="selectpicker show-tick form-control" > </select>
                                        </div>
                                        <div >
                                            Materia:     
                                            <select name="SelectMateria" id="SelectMateria" onchange="SeleccionandoMateria('SelectFacultad','SelectPrograma',this);"class="selectpicker show-tick form-control" > </select>

                                        </div>


                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                    <p></p>
                    <div class="tablaDocumentoBusquedaNormal">
                        <table class="table table-bordered">
                            <thead>
                                <tr class ="danger">
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Docente
                                    </th>
                                    <th>
                                        Programa
                                    </th>
                                    <th>
                                        Materia
                                    </th>
                                    <th>
                                        N.Documento
                                    </th>
                                    <th>
                                        Origen
                                    </th>
                                    <th>
                                        valor
                                    </th>
                                    <th>
                                        Fecha
                                    </th>
                                    <th>
                                        Nota
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="warning">
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        Jeison diaz
                                    </td>
                                    <td>
                                        Ing Sistemas
                                    </td>
                                    <td>
                                        Taller software
                                    </td>
                                    <td>

                            <u><a href="#"onclick=" Documento()">Implementacion web</a></u>

                            </td>
                            <td>
                                Copimar 
                            </td>
                            <td>
                                $ 1.000 
                            </td>
                            <td>
                                28/abril/2014 
                            </td>
                            <td>
                                enviar taller sobre esto.. 
                            </td>

                            </tr>
                            <tr class="warning">
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                                <td>

                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>




                </div>


                <div class="cajaDocente" >

                    <div class="LoginDocente">
                        <div class="panel panel-default">
                            <div class="panel-body">


                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Login</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="nameDocente" placeholder="Ingresar Nick" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="password" placeholder="Ingresar Clave"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" onclick="loginDocente()" class="btn btn-default">Ingresar</button>
                                            <span class="label label-danger" id="message"> </span>
                                        </div>
                                        <p>
                                            <a class="btn" href="#">¿No dispones de una cuenta? Regístrate ahora</a><a class="btn btn-primary " onclick="loginDocente()" href="#">Regístrar</a> 
                                        </p>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="MenuDocente">
                        <div class="container">
                            <div class="row clearfix">
                                <div class="col-md-12 column">
                                    <div class="tabbable" id="tabs-913328">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#panel-187645" data-toggle="tab">Iniciar</a>
                                            </li>
                                            <li>
                                                <a href="#panel-951580" data-toggle="tab">Todos los Documentos</a>
                                            </li>
                                            <li>
                                                <a href="#panel-951581" data-toggle="tab" onclick="SubirDocumento()">Subir</a>
                                            </li>
                                            <li>
                                                <a href="#panel-951582" data-toggle="tab">Personal</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content"  >
                                           
                                            
                                            <div class="tab-pane active" id="panel-187645" >
                                                <p>
                                                   iniciar
                                                </p>

                                                <b><span class="azul"><h1>Buscar :</h1></span></b>
                                                <input type="text"class="form-control" placeholder="Buscar documento" >
                                                <label  class="col-sm-2 control-label">Por fecha</label>
                                                <input type="text" class="form-control" placeholder="Ingresar Fecha">

          
                                                  <div >

                                                    <table class="table"  id="tablaMostrarTodosLosDocumento">
                                                        <thead>
                                                            <tr>

                                                                <th>
                                                                    Docente
                                                                </th>
                                                                <th>
                                                                    Materia
                                                                </th>
                                                                <th>
                                                                    Documento
                                                                </th>
                                                                <th>
                                                                    Origen
                                                                </th>
                                                                <th>
                                                                    Ingreso
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="cuerpotablaMostrarTodosLosDocumento">
                                                            <tr>
                                                                <td>
                                                                    jeison
                                                                </td>
                                                                <td>
                                                                    taller sw
                                                                </td>
                                                                <td>
                                                                    casos_de_uso.pdf
                                                                </td>
                                                                <td>
                                                                    Copimar

                                                                </td>
                                                                <td>
                                                                    01/04/2012
                                                                </td>
                                                            </tr>
                                                            <tr class="active">
                                                                <td>
                                                                    1
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    01/04/2012
                                                                </td>
                                                                <td>
                                                                    Approved
                                                                </td>
                                                            </tr>
                                                            <tr class="success">
                                                                <td>
                                                                    2
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    02/04/2012
                                                                </td>
                                                                <td>
                                                                    Declined
                                                                </td>
                                                            </tr>
                                                            <tr class="warning">
                                                                <td>
                                                                    3
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    03/04/2012
                                                                </td>
                                                                <td>
                                                                    Pending
                                                                </td>
                                                            </tr>
                                                            <tr class="danger">
                                                                <td>
                                                                    4
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    04/04/2012
                                                                </td>
                                                                <td>
                                                                    Call in to confirm
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>



                                                </div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                

                                            </div>
                                            <div class="tab-pane" id="panel-951580">
                                                <p>
                                                    Estos son todos  documentos
                                                </p>
                                                <div >

                                                    <table class="table"  id="tablaMostrarTodosLosDocumento">
                                                        <thead>
                                                            <tr>

                                                                <th>
                                                                    Docente
                                                                </th>
                                                                <th>
                                                                    Materia
                                                                </th>
                                                                <th>
                                                                    Documento
                                                                </th>
                                                                <th>
                                                                    Origen
                                                                </th>
                                                                <th>
                                                                    Ingreso
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="cuerpotablaMostrarTodosLosDocumento">
                                                            <tr>
                                                                <td>
                                                                    jeison
                                                                </td>
                                                                <td>
                                                                    taller sw
                                                                </td>
                                                                <td>
                                                                    casos_de_uso.pdf
                                                                </td>
                                                                <td>
                                                                    Copimar

                                                                </td>
                                                                <td>
                                                                    01/04/2012
                                                                </td>
                                                            </tr>
                                                            <tr class="active">
                                                                <td>
                                                                    1
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    01/04/2012
                                                                </td>
                                                                <td>
                                                                    Approved
                                                                </td>
                                                            </tr>
                                                            <tr class="success">
                                                                <td>
                                                                    2
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    02/04/2012
                                                                </td>
                                                                <td>
                                                                    Declined
                                                                </td>
                                                            </tr>
                                                            <tr class="warning">
                                                                <td>
                                                                    3
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    03/04/2012
                                                                </td>
                                                                <td>
                                                                    Pending
                                                                </td>
                                                            </tr>
                                                            <tr class="danger">
                                                                <td>
                                                                    4
                                                                </td>
                                                                <td>
                                                                    TB - Monthly
                                                                </td>
                                                                <td>
                                                                    04/04/2012
                                                                </td>
                                                                <td>
                                                                    Call in to confirm
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>



                                                </div>

                                            </div>
                                            <div class="tab-pane" id="panel-951581" >
                                                <p>
                                                    Subir Archivos

                                                </p>

                                                <b><span class="azul"><h1>Materia</h1></span></b>
                                                
                                                <select  id="SelectMateriaDelDocente" class="selectpicker show-tick form-control" > </select>


                                                <b><span class="azul"><h1>Nombre documento</h1></span></b>
                                                <input class="form-control" id="NombreDocumento" type="text" placeholder="Ingrese Nombre">
                                                <b><span class="azul"><h1>Fotocopiadora A quien desea Subirlo</h1></span></b>

                                                <select  id="SelectFotocopiadora" onchange="SeleccionandoPrograma(this);" class="selectpicker show-tick form-control" > </select>

                                                <b><span class="azul"><h1>Selecciones Archivo</h1></span></b>
                                                <input type="file" id="exampleInputFile">
                                                <P> </P> 
                                                <button type="submit"  onclick="guardarDocumento('SelectMateriaDelDocente','SelectFotocopiadora')" class="btn btn-default">Guardar</button>
                                                <div class="row clearfix">
                                                    <div class="col-md-12 column">
                                                        <blockquote>
                                                            <p>


                                                                este documento sera guardado en la base de datos y asignado al centro de copiado para su respectiva reproduccion
                                                            </p> 
                                                        </blockquote>
                                                    </div>
                                                </div>

                                            </div>



                                            <div class="tab-pane" id="panel-951582">
                                                <p>
                                                    personal
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="RegistroDocente">
                        <div class="panel panel-default">
                            <div class="panel-body"> 
                                <label for="exampleInputEmail1">Nombre</label>
                                <input class="form-control" type="text" placeholder="Ingrese Nombre">

                                <label for="exampleInputEmail1">Primer Apellido</label>
                                <input class="form-control" type="text" placeholder="Ingrese Apellido">

                                <label for="exampleInputEmail1">Segundo Apellido</label>
                                <input class="form-control" type="text" placeholder="Ingrese Apellido">

                                <label for="exampleInputEmail1">Crear Login</label>
                                <input class="form-control" type="text" placeholder="Ingrese Login">

                                <label for="exampleInputEmail1">Crear Clave</label>
                                <input class="form-control" type="text" placeholder="Ingrese Clave">

                                <label for="exampleInputEmail1">Telefono</label>
                                <input class="form-control" type="text" placeholder="Ingrese Telefono fijo/Celular">

                                <label for="exampleInputEmail1">Seleccione Facultad</label>
                                <select class="form-control">
                                    <option>Ingenieria</option>
                                    <option>Empresarial</option>
                                    <option>Sulud</option>

                                </select>
                                <label for="exampleInputEmail1">Agregando Facultades</label>
                                <select multiple class="form-control" >
                                    <option>Ingenieria</option>
                                    <option>Empresarial</option>
                                    <option>Sulud</option>

                                </select>
                                <button type="submit" onclick="" class="btn btn-default">Quitar</button>

                                <br>
                                <label for="exampleInputEmail1">Seleccione los Programa que pertenece</label>
                                <select multiple class="form-control">
                                    <option>sistemas</option>
                                    <option>Industrial</option>
                                    <option>Contaduria</option>

                                </select>
                                <label for="exampleInputEmail1">Agregando Programas</label>
                                <select multiple class="form-control">
                                    <option>-----------</option>

                                </select>
                                <button type="submit" onclick="" class="btn btn-default">Quitar</button>
                                </br>
                                <label for="exampleInputEmail1">Seleccione las Materias que dicta</label>
                                <select multiple class="form-control">
                                    <option>algoritmos</option>
                                    <option>Diferencial</option>
                                    <option>Integral</option>
                                    <option>Estadisticas</option>
                                    <option>Analisis N</option>
                                </select>
                                <label for="exampleInputEmail1">Agregando Materias</label>
                                <select multiple class="form-control">
                                    <option>-----------</option>
                                </select>
                                <button type="submit" onclick="" class="btn btn-default">Quitar</button>


                                </br>

                                <div class="form-group">
                                    <br>
                                    <label for="exampleInputFile">Guardar</label>
                                    </br>

                                    <a class="btn btn-primary " onclick="Docente()" href="#">Regístrar</a> 

                                </div>
                                </br>


                            </div>
                        </div>
                    </div>
                </div>


                <div class="cajaAdministrador" >

                    <!-- //embed src="img/IHMCCmapTools-Help.pdf#toolbar=0" width="950" height="700" //esto es para mostrar un pdf embed src=”manual.pdf#toolbar=0″ width=”500″ height=”375″-->

                    <div class="LoginAdmin">
                        <div class="panel panel-default">
                            <div class="panel-body">


                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Login</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputPassword3" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" onclick="loginAdministrador()" class="btn btn-default">Ingresar</button>
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>





                </div>


            </div>
    </body>
</html>
